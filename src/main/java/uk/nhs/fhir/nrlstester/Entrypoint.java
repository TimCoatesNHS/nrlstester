/*
 * Copyright 2017 NHS Digital
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.nrlstester;

import uk.nhs.fhir.nrlstester.utils.PropertyReader;
import uk.nhs.fhir.nrlstester.interceptor.NRLSClientFactory;
import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import ca.uhn.fhir.model.dstu2.resource.OperationOutcome;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.client.ServerValidationModeEnum;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.logging.Logger;
import org.hl7.fhir.instance.model.api.IIdType;
import uk.nhs.fhir.nrlstester.utils.DocRefFactory;

/**
 * Simple code to test against the NRLS whether the 'live' one or to run the same tests against any FHIR DSTU2 server.
 *
 * @author tim.coates@hscic.gov.uk
 */
public class Entrypoint {

    private static final Logger LOG = Logger.getLogger(Entrypoint.class.getName());
    private static final String serverBase = PropertyReader.getProperty("ServerBase");

    /**
     * Command line entry point
     *
     * @param args commandline arguments.
     */
    public static void main(String[] args) {
        Entrypoint instance = new Entrypoint();
        instance.run();
    }

    /**
     * Our non-static entrypoint.
     *
     * Here we initialise, add 'n' DocRef resources, search for them, then delete them all.
     *
     */
    private void run() {
        ArrayList<String> records = new ArrayList<String>();

        //////////////////////////////////////////////////////////////////////////////
        // First we try to initialise, creating a Client which authenticates etc
        IGenericClient client = initialise();

        if(client != null) {
            LOG.info("Initialised");

            //////////////////////////////////////////////////////////////////////////////
            // Get the number of DocumentRefeence objects we'll write, read and later delete...
            int Count = Integer.parseInt(PropertyReader.getProperty("Count"));
            LOG.info("Adding " + Count + " DocumentReference Records...");

            String NHSNumber = PropertyReader.getProperty("NHSNumber");
            
            //////////////////////////////////////////////////////////////////////////////
            // Try to read in the Bundle we've saved via curl...
            //LOG.info("Loaded a bundle containing: " + ReadSavedBundleFile("c:/users/tico3/nrlsssl/file.txt"));
            
            //////////////////////////////////////////////////////////////////////////////
            // This is temporary to try and delete a specific record!
            //deleteByPatAndMasterID("https%3A%2f%2fpds.proxy.nhs.uk%2fPatient%2f" + NHSNumber, "f3b69920-6a94-4af0-b02c-86c38ce71fa5");


            //////////////////////////////////////////////////////////////////////////////
            // Add the records
            for(int i = 0; i < Count; i++) {
                boolean optionals = false;
                if((i % 2) == 0) {
                    optionals = true;
                }
                optionals=true;
                IIdType idValue = addRecord(client, optionals, NHSNumber);
                if(idValue != null) {
                    String idString = idValue.toString();

                    if(getRecord(client, idString)) {
                        LOG.info("Got the record back");
                    } else {
                        LOG.info("Couldn't get the record");
                    }
                    records.add(idString);
                    LOG.info("Added: " + idString);
                }
            }
            

            //////////////////////////////////////////////////////////////////////////////
            // See how many records exist for the specified patient...
            int findCount = findRecords(client, NHSNumber);
            if(findCount == Count) {
                LOG.info("Found what we saved");
            } else {
                LOG.warning("Found " + findCount + " but expected " + Count + " records?");
            }

            //////////////////////////////////////////////////////////////////////////////
            // Delete the records we created
            for(String url : records) {
                LOG.info("Deleting " + url);
                deleteRecord(client, url);
            }

            //////////////////////////////////////////////////////////////////////////////
            // Clean up, deleting any remnants
            //deleteAllRecords(client, NHSNumber);

        } else {
            LOG.warning("Failed to initialise");
        }
    }

    /**
     * Initialise our FHIR client object, based on the URL fetched from config.properties file.
     *
     * @return boolean indication of success
     */
    private IGenericClient initialise() {
        IGenericClient newClient = null;

        // We're connecting to a DSTU2 compliant server in this example
        FhirContext ctx = FhirContext.forDstu2();
        // TODO: Remove this once ConformanceProfile is available
        ctx.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
        newClient = NRLSClientFactory.getNRLSClient(ctx);
        
        newClient.setLogRequestAndResponse(true);

        if(newClient == null) {
            LOG.severe("Failed to get a working client");
        }
        return newClient;
    }

    /**
     * Adds a record, for the given NHS Number with optional fields either present or missing depending on the supplied value.
     *
     * @param optionals Do we want the optional fields, or just the minimum?
     * @return The ID of the created resource.
     */
    private IIdType addRecord(IGenericClient client, boolean optionals, String NHSNumber) {
        IIdType reference = null;
        DocumentReference ref = DocRefFactory.makeDR(serverBase, optionals, NHSNumber);
        try {
            LOG.info("\n\n+++++++++++++++++++\nAdding a record...");
            MethodOutcome mOut = client.create().resource(ref).execute();
            if(mOut != null) {
                if(mOut.getCreated())
                    LOG.info("Created a resource");
                else
                    LOG.severe("Ignore the location header, Failed to create!!");
                reference = mOut.getId().toVersionless();
            }
        } catch (InvalidRequestException ex) {
            LOG.severe(ex.getMessage());
        }
        return reference;
    }

    /**
     * Searches for records for the specified NHS Number.
     *
     * @return
     */
    private int findRecords(IGenericClient client, String NHSNumber) {
        int records = 0;
        String searchUrl = serverBase + "/DocumentReference?subject=https3A%2F%2Fpds.proxy.nhs.uk%2FPatient%2F" + NHSNumber;

        LOG.info("\n\n+++++++++++++++++++\nFinding records...");
        // Perform a search
        Bundle results = null;
        try {           
            results = client.search()
                    .byUrl(searchUrl)
                    //.usingStyle(SearchStyleEnum.POST)
                    .returnBundle(ca.uhn.fhir.model.dstu2.resource.Bundle.class)
                    .execute();
            records = results.getEntry().size();
            System.out.println("Found " + results.getEntry().size() + " DocumentReferences");
        } catch (Exception e) {
            LOG.severe(e.getClass().getName() + " : " + e.getMessage());
        }
        LOG.info("\nfindRecords found: " + records + " records\n");
        return records;
    }

    /**
     * Deletes a DocRef based on it's Identifier
     *
     * @param url
     */
    private void deleteRecord(IGenericClient client, String url) {
        IdDt theId = new IdDt(url);
        OperationOutcome oo = null;
        LOG.info("\n\n+++++++++++++++++++\nDeleting a record...");
        try {
            oo = (OperationOutcome) client.delete().resourceById(theId).execute();
        } catch (Exception e) {
            LOG.info(e.getMessage());
        }
        LOG.info(url);
    }

    /**
     * Deletes all documentReferences for a Patient based on NHS Number
     *
     * @param NHSNumber The NHS Number of the Patient
     */
    private void deleteAllRecords(IGenericClient client, String NHSNumber) {
        int records = 0;
        String searchUrl = serverBase + "/DocumentReference?subject.identifier=http://fhir.nhs.net/Id/nhs-number|" + NHSNumber;
        searchUrl = serverBase + "/DocumentReference?subject=https://pds.proxy.nhs.uk/Patient/" + NHSNumber;
        
        // Perform a search
        try {
            Bundle results = client.search()
                    .byUrl(searchUrl)
                    .returnBundle(ca.uhn.fhir.model.dstu2.resource.Bundle.class)
                    .execute();

            records = results.getEntry().size();

            LOG.info("Will now also clean up: " + records + " records for patient with NHS Number: " + NHSNumber);

            for(int i = 0; i < results.getEntry().size(); i++) {
                String drURL = results.getEntry().get(i).getFullUrl();
                deleteRecord(client, drURL);
            }
        } catch (Exception e) {
            LOG.severe(e.getClass().getName() + " : " + e.getMessage());
        }
    }

    /**
     * Method to try and retrieve a specified DocumentReference object back from the server.
     *
     * @param recordURL the URL (relative to the server base) of the DocumentReference
     * @return true if it was successfully retrieved
     */
    private boolean getRecord(IGenericClient client, String recordURL) {
        boolean success = false;
        String searchUrl = serverBase + recordURL;
        try {
            DocumentReference record = (DocumentReference) client.fetchResourceFromUrl(DocumentReference.class, searchUrl);
            success = true;
        } catch (Exception e) {
            LOG.severe("Failed to retrieve record from: " + searchUrl);
        }
        return success;
    }
    
    private void deleteByPatAndMasterID(IGenericClient client, String Patient, String MasterId) {
        String delURL = "DocumentReference?subject=" + Patient + "%26masterIdentifier=" + MasterId;
        OperationOutcome execute = (OperationOutcome) client.delete().resourceConditionalByUrl(delURL).execute();
        LOG.severe(execute.getText().toString());
        return;
    }
    
    /**
     * Diagnosing problems - method to read in a bundle which we downloaded via curl...
     * 
     * @param filename The name of the file we've downloaded.
     * @return The number of resources in the bundle.
     */
    private int ReadSavedBundleFile(FhirContext ctx, String filename) {
        int count = 0;
        try {
            Reader targetReader = new FileReader(filename);
            ca.uhn.fhir.model.api.Bundle bndl = ctx.newXmlParser().parseBundle(targetReader);
            targetReader.close();
            count = bndl.size();
        } catch (IOException ex) {
            LOG.severe("IOException when trying to read file: " + filename + "\n" + ex.getMessage());
        }
        return count;
    }
}
