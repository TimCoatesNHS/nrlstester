/*
 * Copyright 2017 tim.coates@hscic.gov.uk.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.nrlstester.utils;

import ca.uhn.fhir.model.dstu2.composite.AttachmentDt;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.resource.DocumentReference;
import ca.uhn.fhir.model.dstu2.valueset.DocumentReferenceStatusEnum;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import ca.uhn.fhir.model.primitive.InstantDt;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Class which generates a DocumentReference for us
 * 
 * @author tim.coates@hscic.gov.uk
 */
public class DocRefFactory {
    
    /**
     * Generates a new DocumentReference object with some fixed values, and some variable.
     *
     * @param ServerBase The server base URL we build resource references pointing to.
     * @param optionals Do we want all the optional fields too, or only the mandatory ones?
     * @param NHSNumber NHS Number of the patient we want the DocRef to be about.
     * @return
     */
    public static DocumentReference makeDR(String ServerBase, boolean optionals, String NHSNumber) {
        DocumentReference ref = new DocumentReference();
        String masterIdentifier = UUID.randomUUID().toString();

        // Set the MasterIdentifier
        // 1..1
        IdentifierDt mi = new IdentifierDt("", masterIdentifier);
        ref.setMasterIdentifier(mi);

        // Set the Subject
        // 1..1
        ref.getSubject().setReference(ServerBase + "/Patient/" + NHSNumber);
        //ref.getSubject().setReference(ServerBase + "/Patient/1234567890");

        // Set the type
        // 1..1
        CodeableConceptDt type = new CodeableConceptDt();
        CodingDt typeCoding = new CodingDt();
        typeCoding.setDisplay("End of Life Care Coordination Summary");
        typeCoding.setSystem("http://snomed.info/sct");
        typeCoding.setCode("861421000000109");
        ArrayList<CodingDt> typesList = new ArrayList<CodingDt>();
        typesList.add(typeCoding);
        type.setCoding(typesList);
        ref.setType(type);

        // Set the author
        // 1..1
        ResourceReferenceDt author = new ResourceReferenceDt();
        author.setDisplay("HONEYMAN S");
        author.setReference("http://phh-l-iopweb-l1:8080/hapi-fhir-jpas/Practitioner/4");
        ArrayList<ResourceReferenceDt> authorList = new ArrayList<ResourceReferenceDt>();
        authorList.add(author);
        ref.setAuthor(authorList);

        // Set the custodian
        // 1..1
        ResourceReferenceDt custodian = new ResourceReferenceDt();
        custodian.setDisplay("Silverdale Family Practice");
        custodian.setReference("https://sds.proxy.nhs.uk/Organization/1");
        ref.setCustodian(custodian);

        // Set the authenticator
        // 0..1
        if(optionals) {
            ResourceReferenceDt authenticator = new ResourceReferenceDt();
            authenticator.setDisplay("VARLEY SW");
            authenticator.setReference("http://phh-l-iopweb-l1:8080/hapi-fhir-jpas//Practitioner/545");
            ref.setAuthenticator(author);
        }

        // Set the created date time
        // 1..1
        DateTimeDt created = new DateTimeDt("2016-03-08T15:26:00+01:00");
        ref.setCreated(created);

        // Set the indexed date time
        // 1..1
        InstantDt indexed = new InstantDt("2017-01-01T13:28:00+01:00");
        ref.setIndexed(indexed);

        // Set the status
        // 1..1
        ref.setStatus(DocumentReferenceStatusEnum.CURRENT);

        // Set the description
        // 0..1
        if(optionals) {
            ref.setDescription("Descriptive text about document " + masterIdentifier + " goes here");
        }

        // Set the securityLabel
        // 1..1
        ArrayList<CodeableConceptDt> secLabelList = new ArrayList<CodeableConceptDt>();
        CodeableConceptDt secLabel = new CodeableConceptDt();
        CodingDt secLabelCode = new CodingDt();
        secLabelCode.setCode("V");
        secLabelCode.setSystem("http://hl7.org/fhir/ValueSet/security-labels");
        secLabelCode.setDisplay("very restricted");
        ArrayList<CodingDt> codingList = new ArrayList<CodingDt>();
        codingList.add(secLabelCode);
        secLabel.setCoding(codingList);
        secLabelList.add(secLabel);
        ref.setSecurityLabel(secLabelList);

        // Set the content
        // 1..1
        ResourceReferenceDt docRef = new ResourceReferenceDt();
        AttachmentDt attachment = new AttachmentDt();
        attachment.setContentType("application/pdf");
        attachment.setUrl("http://data.developer.nhs.uk/fhir/nrls-v1-draft-a/Profile.RecordLocator/nrls-documentreference-1-0.html");
        attachment.setSize(1234);
        attachment.setTitle("NRLS Record Locator Service - FHIR Implementation Guide");
        DocumentReference.Content content = new DocumentReference.Content();
        content.setAttachment(attachment);
        ref.addContent(content);

        // Set the facility type
        // 1..1
        CodeableConceptDt facility = new CodeableConceptDt();
        CodingDt facilityCode = new CodingDt();
        facilityCode.setCode("83891005");
        facilityCode.setSystem("http://snomed.info/sct");
        facilityCode.setDisplay("Solo practice private office");
        facility.addCoding(facilityCode);
        DocumentReference.Context cntxt = new DocumentReference.Context();
        cntxt.setFacilityType(facility);
        ref.setContext(cntxt);
        //LOG.info(ctx.newJsonParser().setPrettyPrint(true).encodeResourceToString(ref));

        return ref;
    }
}
