/*
 * Copyright (C) 2016 Health and Social Care Information Centre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.nrlstester.interceptor;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.IGenericClient;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import uk.nhs.fhir.nrlstester.utils.PropertyReader;

/**
 * Factory class which generates a Client (of type: ca.uhn.fhir.rest.client.IGenericClient) for
 * connecting to the NRLS. This client is specialised to:
 *    Use a spine issued certificate
 *    Use that certificate to do TLS/MA
 *    Add the fromASID, toASID, userID, roleProfileID and traceID headers required by spine
 *    Remove the Accept-Encoding: gzip header
 *
 * @author Tim.Coates@hscic.gov.uk
 */
public class NRLSClientFactory {
    private static final Logger LOG = Logger.getLogger(NRLSClientFactory.class.getName());
    private static final MyHttpRequestInterceptor requestInterceptor = new MyHttpRequestInterceptor();

    /**
     * Generate a new client to connect to the NRLS.
     *
     * @param ctx An existing FHIRContext.
     * @param hostName The FHIR server we're trying to connect to.
     * @return A IGenericClient which will insert the necessary headers into all requests to the NRLS.
     *
     */
    public static IGenericClient getNRLSClient(FhirContext ctx) {
        IGenericClient genericClient = null;

        // First we get the filenames and passwords for the Keystore and TrustStore files
        String TruststoreFileName = PropertyReader.getProperty("TruststoreFileName");
        String KeystoreFileName = PropertyReader.getProperty("KeystoreFileName");
        String TrustStorePW = PropertyReader.getProperty("TrustStorePW");
        String KeyStorePW = PropertyReader.getProperty("KeyStorePW");

        // Create them both
        KeyStore trustStore = LoadAStore(TruststoreFileName, TrustStorePW);
        KeyStore keyStore = LoadAStore(KeystoreFileName, KeyStorePW);

        if(trustStore != null && keyStore != null) {
            // And create a client that will use them to authenticate...
            genericClient = buildHttpClient(trustStore, keyStore, ctx);
        } else {
            LOG.info("Unable to create a client as I couldn't create an instance of a PKCS12 trustStore");
        }
        return genericClient;
    }

    //<editor-fold defaultstate="collapsed" desc="Private methods wrapped in here">
    
    /**
     * Makes a java.security.KeyStore object from a file, given a filename and password for that file.
     *
     * @param TruststoreFileName Name of a .jks key store file
     * @param TrustStorePW The password to the file.
     * @return A java.security.KeyStore object loaded from the specified file, or null.
     */
    private static KeyStore LoadAStore(String TruststoreFileName, String TrustStorePW) {
        KeyStore trustStore = getTrustStore();
        File fin = new File(TruststoreFileName);
        FileInputStream instream = null;
        try {
            instream = new FileInputStream(fin);
        } catch (FileNotFoundException ex) {
            LOG.severe("FileNotFoundException caught when trying to read from file: " + TruststoreFileName);
        }
        
        if(instream != null) {
            try {
                trustStore.load(instream, TrustStorePW.toCharArray());
            } catch (IOException ex) {
                LOG.severe("IOException caught when trying to load TrustStore from: " + TruststoreFileName + " : " + ex.getMessage());
            } catch (NoSuchAlgorithmException ex) {
                LOG.severe("NoSuchAlgorithmException caught when trying to load TrustStore from: " + TruststoreFileName + " : " + ex.getMessage());
            } catch (CertificateException ex) {
                LOG.severe("CertificateException caught when trying to load TrustStore from: " + TruststoreFileName + " : " + ex.getMessage());
            } finally {
                try {
                    instream.close();
                } catch (IOException ex) {
                    LOG.severe("IOException caught when trying to CLOSE TrustStore at: " + TruststoreFileName + " : " + ex.getMessage());
                }
            }
        }
        return trustStore;
    }
    
    /**
     * Creates the restful client we're going to be using from now on, which knows how to do all of the required SSL stuff.
     *
     * @param trustStore Our Trust Store
     * @param keyStore Our Key store
     * @param ctx The FHIR Context we're working with
     * @return Either a client which works, or null
     */
    private static IGenericClient buildHttpClient(KeyStore trustStore, KeyStore keyStore, FhirContext ctx) {
        IGenericClient genericClient = null;
        String hostName = PropertyReader.getProperty("ServerBase");
        String keyAlias = PropertyReader.getProperty("Alias");
        String AliasPW = PropertyReader.getProperty("AliasPW");
        try {
            if(keyStore.containsAlias(keyAlias)) {
                SSLContext sslcontext = null;
                try {
                    keyStore.getKey(keyAlias, AliasPW.toCharArray());
                } catch (NoSuchAlgorithmException ex) {
                    LOG.severe("NoSuchAlgorithmException caught: " + ex.getMessage());
                } catch (UnrecoverableKeyException ex) {
                    LOG.severe("UnrecoverableKeyException caught: " + ex.getMessage());
                }
                try {
                    SSLContextBuilder builder = new SSLContextBuilder();
                    builder.loadTrustMaterial(trustStore);
                    builder.loadKeyMaterial(keyStore, AliasPW.toCharArray());
                    sslcontext = builder.build();
                } catch (NoSuchAlgorithmException ex) {
                    LOG.severe("NoSuchAlgorithmException caught when trying to create SSLContext: " + ex.getMessage());
                } catch (KeyStoreException ex) {
                    LOG.severe("KeyStoreException caught when trying to create SSLContext: " + ex.getMessage());
                } catch (UnrecoverableKeyException ex) {
                    LOG.severe("UnrecoverableKeyException caught when trying to create SSLContext: " + ex.getMessage());
                } catch (KeyManagementException ex) {
                    LOG.severe("KeyManagementException caught when trying to create SSLContext: " + ex.getMessage());
                }
                if(sslcontext != null) {
                    genericClient = replaceWithSSLClient(sslcontext, ctx, hostName);
                }
            } else {
                LOG.severe("No certificate found in keystore with alias " + keyAlias);
            }
        } catch (KeyStoreException ex) {
            LOG.severe("KeyStoreException caught - keystore was either not read, or didn't contain a certificate for the alias: " + keyAlias + " : " + ex.getMessage());
        }
        return genericClient;
    }
    
    /**
     * Makes the client we're using do all of the necessary TLS/MA certificate stuff
     *
     * @param sslcontext A created SSLContext
     * @param ctx The FHIR Context we're working with
     * @param hostName The hostname we want to connect to
     * @return Either a client doing TLS/MA or null
     */
    private static IGenericClient replaceWithSSLClient(SSLContext sslcontext, FhirContext ctx, String hostName) {
        IGenericClient genericClient;
        
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
        
        // We're basing this on a Custom http request
        HttpClientBuilder builder = HttpClients.custom();
        
        // We tell the socket builder how to do SSL 'our way'
        builder = builder.setSSLSocketFactory(sslsf);
        
        // We tell the socket builder we don't want compressed content
        // This is to work around how spine implemented Gzip
        builder = builder.disableContentCompression();
        
        // Here we add an interceptor in to fine tune each http request...
        builder = builder.addInterceptorLast(requestInterceptor);
        
        // And finally we build a http client with all of that included
        CloseableHttpClient myNewClient = builder.build();
        
        /*  NB: all of thta could have been done as:
        CloseableHttpClient myNewClient = HttpClients
                .custom()
                .setSSLSocketFactory(sslsf)
                .disableContentCompression()
                .addInterceptorLast(requestInterceptor)
                .build();
        */
        
        genericClient = specialiseClient(ctx, myNewClient, hostName);
        return genericClient;
    }
    
    /**
     * Adds the interceptor to the client, so that all requests will include the required headers.
     *
     * @param ctx The FHIR context we're working with
     * @param myNewClient The client we're specialising
     * @param hostName The hostname we're going to connect to
     * @return Either a client that does the headers or null.
     */
    private static IGenericClient specialiseClient(FhirContext ctx, CloseableHttpClient myNewClient, String hostName) {
        IGenericClient genericClient = null;
        ctx.getRestfulClientFactory().setHttpClient(myNewClient);
        
        genericClient = ctx.newRestfulGenericClient(hostName);
        
        // Create a Spine interceptor, specifying values for the expected headers
        //SpineInterceptor interceptor = new SpineInterceptor();
        
        // Register the interceptor with your client
        //genericClient.registerInterceptor(interceptor);
        return genericClient;
    }
    
    /**
     * Gets an instance of a [default] trust store
     *
     * @return Obviously a PKCS12 trust store, or null.
     */
    private static KeyStore getTrustStore() {
        KeyStore trustStore = null;
        try {
            trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        } catch (KeyStoreException ex) {
            LOG.severe("KeyStoreException caught getting a PKCS12 keystore instance");
        }
        return trustStore;
    }
    
    /**
     * Private constructor, as we don't ever want people instantiating this class.
     */
    private NRLSClientFactory() {
    }
    //</editor-fold>
}
