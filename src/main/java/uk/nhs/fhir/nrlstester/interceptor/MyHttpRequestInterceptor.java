/*
 * Copyright 2017 tim.coates@hscic.gov.uk.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.nhs.fhir.nrlstester.interceptor;

import java.io.IOException;
import java.util.UUID;
import java.util.logging.Logger;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.protocol.HttpContext;
import uk.nhs.fhir.nrlstester.utils.PropertyReader;

/**
 * This class intercepts all http requests we make, in order to 'tune' the
 * http request headers. It also adds in the Spine required headers:
 *      fromASID
 *      toASID
 *      userID
 *      roleProfileID
 *      traceID
 * 
 * @author tim.coates@hscic.gov.uk
 */
public class MyHttpRequestInterceptor implements HttpRequestInterceptor {
    private static final Logger LOG = Logger.getLogger(MyHttpRequestInterceptor.class.getName());
    
    private final String _fromASID;
    private final String _toASID;
    private final String _userId;
    private final String _roleProfileId;

    public MyHttpRequestInterceptor() {
        LOG.fine("SpineInterceptor constructor called SpineInterceptor()");

        // Get the fromASID header value from config.properties file, and check it suits the spine regex
        _fromASID = PropertyReader.getProperty("fromASID");
        if(_fromASID.matches("^[0-9]{12}$") == false) {
            LOG.severe("fromASID failed the regex");
        }

        // Get the toASID header value from config.properties file, and check it suits the spine regex
        _toASID = PropertyReader.getProperty("toASID");
        if(_toASID.matches("^[0-9]{12}$") == false) {
            LOG.severe("toASID failed the regex");
        }

        // Get the userId header value from config.properties file, and check it suits the spine reges
        _userId = PropertyReader.getProperty("userId");
        if(_userId.matches("^[0-9]{12}$") == false) {
            LOG.severe("userID failed the regex");
        }

        // Get the roleProfileId header value from the config.properties file, and check it suits the spine regex
        _roleProfileId = PropertyReader.getProperty("roleProfileId");
        if(_roleProfileId.matches("^[0-9]{12}$") == false) {
            LOG.severe("roleProfileID failed the regex");
        }

        // Check none of them are null.
        if(_fromASID != null && _toASID != null && _userId != null && _roleProfileId != null) {
            if(_fromASID.equals("") || _toASID.equals("") || _userId.equals("") || _roleProfileId.equals("")) {
                LOG.severe("One of the header parameters was empty in config file?\nWe requires:\n\tfromASID\n\ttoASID\n\tuserId\n\troleProfileId\n");
            } else {
                LOG.fine("Created SpineInterceptor for user: "+  _userId);
            }
        } else {
            LOG.severe("Failed to find one of the configuration parameters for headers\nrequires:\nfromASID\ntoASID\nuserId\nroleProfileId");
        }
    }

    @Override
    public void process(HttpRequest hr, HttpContext arg1) throws HttpException, IOException {
        LOG.info("Processing a request");

        // Remove the Accept-Encoding header, so the server doesn't think we'll accept gzip
        while(hr.containsHeader("Accept-Encoding")) {
            hr.removeHeader(hr.getFirstHeader("Accept-Encoding"));
        }

        // Remove the charset header, then specify utf-8
        while(hr.containsHeader("Accept-Charset")) {
            hr.removeHeader(hr.getFirstHeader("Accept-Charset"));
        }
        if(hr.containsHeader("Accept-Charset") == false) {
            hr.addHeader("Accept-Charset", "utf-8");
        }

        // Remove the accept header, then add the one we want
        while(hr.containsHeader("Accept")) {
            hr.removeHeader(hr.getFirstHeader("Accept"));
        }
        if(hr.containsHeader("Accept") == false)
        {
            //hr.addHeader("Accept", "application/xml+fhir;q=1.0, application/json+fhir;q=1.0");
            hr.addHeader("Accept", "application/xml+fhir");
        }

        // Remove the User-Agent header then add a HAPI one.
        while(hr.containsHeader("User-Agent")) {
            hr.removeHeader(hr.getFirstHeader("User-Agent"));
        }
        if(hr.containsHeader("User-Agent") == false) {
            hr.addHeader("User-Agent", "HAPI");
        }

        // Generate a TraceID to identify this request...
        String traceId = UUID.randomUUID().toString();
        // Check it matches the spine regex for a traceid...
        if(traceId.matches("^[a-zA-Z0-9-]{0,36}$") == false){
            LOG.severe("Regex failed for the traceID");
        }

        LOG.info("\n\nRequest intercepted:\n\tTraceID: " + traceId + "\n\tURL:     " + hr.getRequestLine() + "\n\n");
        hr.addHeader("fromASID", _fromASID);
        hr.addHeader("toASID", _toASID);
        hr.addHeader("traceId", traceId);
        hr.addHeader("userId", _userId);
        hr.addHeader("roleProfileId", _roleProfileId);
    }
    
}
