# README #

NRLSTester is a simple project built to prove connectivity to the spine proof of concept National Record Locator Service (NRLS) running on the NFT environment.

# What is this repository for? #

##Quick summary##
This project is designed to meet two objectives.

* To allow testing of the NRLS using HAPI client library, in order to verify the interface conforms as closely as possible / necessary to the FHIR specification.
This is required so that:
    * Consuming system suppliers can use the HAPI library (or other FHIR libraries and helper code) as a starting point with confidence that it will successfully connect to the NRLS.
    * We know that our definition of how the interface behaves is consistent, coherent and factually correct.


* To illustrate how the HAPI client can be *coerced* to implement the required TLS/MA authentication in order to connect to the spine NRLS.

##Version##
Version 1.0 so far, no earlier revisions released.

## How do I get set up? ##

### Getting the TLS/MA certificates sorted ###

In order to connect to the NFT (or other) spine environment, we need to use a TLS/MA connection presenting a client certificate issued to us by the SA Service Desk.
In order to do this, we need to set up two Java Keystore files as described below:

 * One Key Store which contains our **Private Key** and the **Client Certificate** we are issued with.
 * One Trust Store which contains the certificates we need to trust, this will be the spine **Root CA** and the **Sub CA** which was actually used to issue our client certificate.

The text below describes the steps required to go about setting these up. This worked for me, but *your mileage may vary*.

  * Get a static N3 facing IP address.
    * You might not need to connect from this IP address, but it's needed for the intermediate steps below.

* Get a DNS entry for that IP address.
    * The form to get a DNS entry is [here](https://digital.nhs.uk/Networking-addressing/Domain-names)

  * Generate a Private Key (on the machine in question)
    * `openssl genrsa -out my_key_file.key 2048`
    * This will generate a private key in the file **my_key_file.key**

  * Generate a Certificate Signing Request (CSR)
    * `openssl req -new -sha1 -key ./my_key_file.key -out ./my_csr_file.csr`
    * This generates the CSR in the file **my_csr_file.csr**

  * Request a client certificate
    * This can be requested from [the SA Service Desk](sa.servicedesk@nhs.net)

  * You should get back a certificate
    * This will probably be embedded in an email which you need to save as (for example) **my_client_cert.crt**

  * Combine your key and the cert into a pfx file
    * `openssl pkcs12 -export -name 1 -in my_client_cert.crt -inkey my_key_file.key -out my_pfx_file.p12`
    * This generates a pkcs12 pfx file containing both your cert and private key **my_pfx_file.p12**
    * You will be asked for the password used when creating your key file.
    * You will also be asked for an optional export password, **you should supply one**.

  * Import your pfx file into a Keystore file
    * `keytool -importkeystore -destkeystore my_keystore_file.jks -srckeystore my_pfx_file.p12 -srcstoretype pkcs12 -alias 1`
    * This will generate a Java Keystore file **my_keystore_file.jks**
    * The Alias of **1** is what the key will be labelled as within the KeyStore file.
    * You will be asked for a password for the new keystore file, and the password for your export above.
    * This Keystore file is what you will need to reference in the **config.properties** file as **KeystoreFileName**

  * Check the key exists in your new keystore file
    * `keytool -list -keystore my_keystore_file.jks`
    * This should list that you have 1 entry in your keystore file with an Alias of **1**
    * `keytool -list -keystore my_keystore_file.jks –alias 1`
    * This would list that entry only

  * Get the **Root** and **SubCA** certificates for the environment you're connecting to
    * These can be downloaded from [here](http://www.assurancesupport.digital.nhs.uk/) in the **downloads** section.

  * Store the root certificate for the environment in a new file
    * Called for example **root_cert.crt**

  * Store the SubCA certificate for the environment in a new file
    * Called for example **subca_cert.crt**

  * Import those certificates into your Trust Store
    * `keytool -import -alias root -keystore my_truststore_file.jks –file root_cert.crt`
    * And
    * `keytool -import –alias subca -keystore my_truststore_file.jks –file subca_cert.crt`
    * This will generate a Java Keystore file **my_truststore_file.jks** containing the certificates your client needs to trust.
    * This is the file you will reference in **config.properties** as **TruststoreFileName**

  * Check the certs exist in your new keystore (truststore) file
    * `keytool -list -keystore my_truststore_file.jks`
    * This should list that you have 2 entry in your keystore file with Aliases of **root** and **subca**

  * If you have problems reading the key from your Keystore or Truststore files, you may have a wrong password for the file, or for the specific alias.
    * To check and/or change this, you can run:
    * `keytool -keypasswd -keystore my_keystore_file.jks -alias 1`
    * This will prompt you to enter the current password for the keystore file then the current password for your specific alias. You can then enter the new password that you wish you use.
    * You can also change the password of the keystore, by running:
    * `keytool -storepasswd -keystore my_keystore_file.jks`

  * For testing, see [this helpful tutorial](https://jamielinux.com/docs/openssl-certificate-authority/introduction.html) on generating root and Sub CA PKI set up.


### Setting up the project ###
* Summary of set up

In order to get this project running, two steps are required:

* Edit the config.properties.template file, and save it as config.properties

* Build and install the project using Maven, all dependencies should be downloaded as required.

* Configuration

As mentioned above, a **template** config.properties file is supplied, with place-holders and descriptions for all required configuration properties.

* Dependencies

Requires Java JDK 1.7
All downloaded using standard maven build and deployment processes, see <dependencies> section in the pom.xml file.

* Deployment instructions

The project can be built using standard maven command line `mvn install`.
Once successfully installed, the resulting jar file can be run.

## Contribution guidelines ##

* Writing tests

Currently no unit tests have been included, they would be more than welcome.

* Code review

Any comments on the code would be gratefully received, including merge requests.

## Who do I talk to? ##

* Repo owner or admin

tim.coates@nhs.net

* Other community or team contact

Also contactable via unofficial email at tim.coates@gmail.com
